import type { Props } from "@/components/BlogCard";
import type { RouteLocationRaw } from "vue-router";

type ListItem = Pick<Props, "describe" | "tags" | "date">;

export interface List extends ListItem {
    title: string;
    href: RouteLocationRaw;
    classes?: (nav: List, index: number) => Array<number | string>;
}

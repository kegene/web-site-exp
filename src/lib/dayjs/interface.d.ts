import type dayjs from "dayjs";

type Dayjs = typeof dayjs;
type DayjsR = ReturnType<Dayjs>;

export type { Dayjs, DayjsR };

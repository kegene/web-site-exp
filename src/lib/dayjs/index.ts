import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import localizedFormat from "dayjs/plugin/localizedFormat";
import zhTW from "dayjs/locale/zh-tw";

dayjs.extend(relativeTime);
dayjs.extend(localizedFormat);

dayjs.locale(zhTW);

export default dayjs;

// import type { InjectionKey } from "vue";
// import type dayjs from "dayjs";

// const $dayjsKey: InjectionKey<typeof dayjs> = Symbol("$dayjs");

// export default $dayjsKey;

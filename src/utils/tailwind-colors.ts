/** @type {import('tailwindcss').Config} */
import colors from "tailwindcss/colors";

type ITwAllColors = typeof colors;
type ITwV2ColorsKey = keyof Pick<
    ITwAllColors,
    "lightBlue" | "warmGray" | "trueGray" | "coolGray" | "blueGray"
>;
type ITwV3Colors = Omit<ITwAllColors, ITwV2ColorsKey>;

export const TwKeys = Object.keys(colors) as (keyof ITwAllColors)[];
export const lowVersion: Array<string | ITwV2ColorsKey> = [
    "lightBlue",
    "warmGray",
    "trueGray",
    "coolGray",
    "blueGray",
];
export const TwV3Colors: ITwV3Colors = TwKeys.reduce((count, key) => {
    if (lowVersion.includes(key)) return count;
    count[key] = colors[key];
    return count;
}, {} as any);

export const TwV3ColorsKey = Object.keys(TwV3Colors) as (keyof ITwV3Colors)[];

/**
 * 用來生成固定 tailwind css Class name，因為 tailwind css 不支持動態命名。
 * @param part ex. text, bg, border
 * @param value color value. ex: 50, 100, 500
 */
export function getColor(part: string[], value: number) {
    type Data<T> = {
        [K in keyof T]?: string; // <part>-<color>-<number>. ex: text-sky-500
    };
    const passList = ["black", "white", "current", "transparent"];
    const obj: Data<ITwV3Colors> = {};
    for (const key of TwV3ColorsKey) {
        if (passList.includes(key)) {
            obj[key] = part
                .reduce((str, partKey) => {
                    str.push([partKey, key].join("-"));
                    return str;
                }, [] as string[])
                .join(" ");
        } else {
            obj[key] = part
                .reduce((str, partKey) => {
                    str.push([partKey, key, value].join("-"));
                    return str;
                }, [] as string[])
                .join(" ");
        }
    }
    return obj;
}

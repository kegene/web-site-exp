import dayjs from "@/lib/dayjs";

const formatTemplate = "L";

const relativeTime = <T extends number | string | Date>(
    start: T,
    end: T,
    short: boolean = false
): string => {
    const endToDayjs = dayjs(end);
    const date = dayjs(start).to(endToDayjs, short);
    return date;
};
const formatDate = <T extends number | string | Date>(
    dates: T[],
    template = formatTemplate
) => {
    if (!dates.length) return [];
    const formatDates = [];

    for (const date of dates) {
        formatDates.push(dayjs(date).format(template));
    }

    return formatDates;
};

export { formatTemplate, relativeTime, formatDate };

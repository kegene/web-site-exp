import TimeLine from "./TimeLine.vue";
import TimelineItem from "./TimeLineItem.vue";

export { TimelineItem };
export default TimeLine;

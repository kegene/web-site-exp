/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: ["./index.html", "./src/**/*.{ts,tsx,js,jsx,vue}"],
  theme: {
    extend: {
      animation: {
        'hue-shift': 'hue-shift 2s linear infinite',
      },
      keyframes: {
        'hue-shift': {
          "0%": {
              filter: 'hue-rotate(0deg)'
          },
          "25%": {
              filter: 'hue-rotate(90deg)',
          },
          "50%": {
              filter: 'hue-rotate(180deg)',
          },
          "75%": {
              filter: 'hue-rotate(270deg)',
          },
          "100%": {
              filter: 'hue-rotate(360deg)',
          },
        },
        'hue-shift-sm': {
          "0%": {
              filter: 'hue-rotate(0deg)'
          },
          "100%": {
              filter: 'hue-rotate(360deg)',
          },
        }
      }
    },
    screens: {
      'max-sm': {'max': '640px'},
      'max-md': {'max': '768px'},
      'max-lg': {'max': '1024px'},
      'max-xl': {'max': '1280px'},
      'max-2xl': {'max': '1536px'},
      ...defaultTheme.screens
    }
  },
  plugins: [
    '@tailwindcss/aspect-ratio'
  ],
};
